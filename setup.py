from setuptools import setup

setup(
    name="lsnrf",
    author="Thomas Stenersen",
    author_email="thomas.stenersen@nordicsemi.no",
    url="https://github.com/thomasstenersen/lsnrf",
    version="0.1.0",
    description="",
    install_requires=[""],
    python_requires=">=3",
    scripts=["bin/lsnrf"]
)
