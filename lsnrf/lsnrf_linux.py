#!/usr/bin/env python3
# MIT License
#
# Copyright (c) 2018 Thomas Stenersen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import subprocess
import re
import os
import sys

from lsnrf.nrf import NRF


def convert(s):
    if sys.version_info < (3, 0):
        return s
    else:
        return str(s, encoding="utf-8")


def get_connected_devices(filt="ttyACM"):
    ttys = [tty for tty in os.listdir("/dev/") if filt in tty]
    dmesg = convert(subprocess.check_output(["dmesg"]))
    regex = "SerialNumber: 000(\d{9})[\n\W\d]+cdc_acm\W[\d\-.:]+:\W(ttyACM\d+)"
    matches = re.finditer(regex, dmesg, re.MULTILINE)
    matches = [(match.group(1), match.group(2)) for match in matches]
    matches.reverse()

    ids = []
    for match in matches:
        segger_id, tty = match
        if not (any(tty in d.com for d in ids) or
                any(d.snr == segger_id for d in ids)) and tty in ttys:
            ids.append(NRF(snr=segger_id, com=("/dev/" + tty)))

    return ids


